@echo off
set BUILD_ROOT=%cd%
@rem set VCPKG_ROOT=%BUILD_ROOT%\vcpkg
@rem call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat"
for %%v in (v142) do (
    cd %BUILD_ROOT%
    mkdir %%v
    xcopy /y /s /d /q /h %VCPKG_ROOT%\* %%v
    cd %%v
    @rem set VCPKG_ROOT=%BUILD_ROOT%\%%v
    copy /y ..\vcpkg-triplets\%%v\* triplets
    if NOT EXIST vcpkg.exe call bootstrap-vcpkg.bat
    set allpkgs=
    setlocal EnableDelayedExpansion
    for /f "delims=" %%t in ('dir /s/b triplets\f8-*.cmake') do (
        @rem echo %%v - %%~nt
        vcpkg.exe install openssl:%%~nt curl:%%~nt tbb:%%~nt poco:%%~nt protobuf:%%~nt zlib:%%~nt zeromq:%%~nt
        set allpkgs=!allpkgs! openssl:%%~nt curl:%%~nt tbb:%%~nt poco:%%~nt protobuf:%%~nt zlib:%%~nt zeromq:%%~nt
    )
    vcpkg.exe export !allpkgs! --7zip
    cd ..
)
cd %BUILD_ROOT%
echo !!! TODO before commiting to LFS !!!
echo 1. Rename vcpkg-export-* to vckpg-export-(v140,v141).7z
echo 2. Open vckpg-export-(v140,v141).7z and rename inner folder from vcpkg-export-* to vcpkg-export, move them to packages folder
echo 3. Commit to LFS and push LFS
@rem copy /y vcpkg-triplets\orig\* %VCPKG_ROOT%\triplets

